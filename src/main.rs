#![allow(dead_code)]

use std::io::Write;

fn main() {
    let mut stdin = Stdin::new();

    assert_eq!(stdin.next::<String>(), "This");
    assert_eq!(stdin.next::<String>(), "is");
    assert_eq!(stdin.next::<String>(), "a");
    assert_eq!(stdin.next::<String>(), "test");
    assert_eq!(stdin.next::<i32>(), 1);
    assert_eq!(stdin.next::<i32>(), 2);
    assert_eq!(stdin.next::<i32>(), 3);

    let mut stdout = Stdout::new();
    writeln!(&mut stdout, "{}", "OK").ok();
}

//------------------------------------------------------------------------------

// Standard Input

struct Stdin {
    buffer: Vec<String>
}

impl Stdin {
    fn new() -> Stdin {
        Stdin {
            buffer: Vec::new(),
        }
    }

    // Adapted from https://codeforces.com/contest/1168/submission/54903799
    fn next<T: std::str::FromStr>(&mut self) -> T {
        loop {
            if let Some(token) = self.buffer.pop() {
                return token.parse().ok().expect("Failed to parse input token");
            }

            let mut line = String::new();
            std::io::stdin().read_line(&mut line).expect("Failed to read from stdin");
            self.buffer.append(&mut line.split_whitespace().rev().map(String::from).collect());
        }
    }
}

// Standard Output

struct Stdout {
    writer: std::io::BufWriter<std::io::Stdout>
}

impl Stdout {
    fn new() -> Stdout {
        Stdout {
            writer: std::io::BufWriter::new(std::io::stdout()),
        }
    }
}

impl std::ops::Deref for Stdout {
    type Target = std::io::BufWriter<std::io::Stdout>;
    fn deref(&self) -> &std::io::BufWriter<std::io::Stdout> {
        &self.writer
    }
}

impl std::ops::DerefMut for Stdout {
    fn deref_mut(&mut self) -> &mut std::io::BufWriter<std::io::Stdout> {
        &mut self.writer
    }
}

// Permutations

struct Permutations<'a, T: 'a + std::cmp::PartialOrd> {
    elements: &'a mut [T],
}

impl<'a, T: 'a + std::cmp::PartialOrd> Permutations<'a, T> {
    fn new(elements: &'a mut [T]) -> Permutations<'a, T> {
        Permutations {
            elements: elements,
        }
    }

    fn get(&self) -> &[T] {
        self.elements
    }

    fn get_mut(&mut self) -> &mut [T] {
        self.elements
    }

    fn next(&mut self) -> bool {
        if self.elements.len() < 2 {
            return false;
        }

        let mut i = self.elements.len() - 1;
        loop {
            i = i - 1;
            if self.elements[i] < self.elements[i + 1] {
                let mut k = self.elements.len() - 1;
                while !(self.elements[i] < self.elements[k]) {
                    k = k - 1;
                }
                self.elements.swap(i, k);
                self.elements[i + 1..].reverse();
                return true;
            }
            if i == 0 {
                self.elements.reverse();
                return false;
            }
        }
    }
}

// Partition

fn partition<T>(slice: &mut [T], p: T) -> (usize, usize)
where
    T: std::cmp::PartialOrd,
{
    let num_lt;
    let num_le;

    unsafe {
        let begin = slice.as_mut_ptr();
        let end = begin.offset(slice.len() as isize);

        let mut ptr = begin;
        let mut eq_end = ptr;
        let mut lt_end = ptr;

        while ptr < end {
            if *ptr < p {
                std::ptr::swap(ptr, lt_end);
                lt_end = lt_end.offset(1);
            } else if *ptr == p {
                std::ptr::swap(ptr, eq_end);
                if lt_end > eq_end {
                    std::ptr::swap(ptr, lt_end);
                }
                eq_end = eq_end.offset(1);
                lt_end = lt_end.offset(1);
            }
            ptr = ptr.offset(1);
        }

        num_lt = (lt_end as usize - eq_end as usize) / std::mem::size_of::<T>();
        num_le = (lt_end as usize - begin as usize) / std::mem::size_of::<T>();

        while eq_end > begin {
            eq_end = eq_end.offset(-1);
            lt_end = lt_end.offset(-1);
            std::ptr::swap(eq_end, lt_end);
        }
    }

    (num_lt, num_le)
}

fn split_at_partitions<T>(slice: &mut [T], p: T) -> (&mut [T], &mut [T], &mut [T])
where
    T: std::cmp::PartialOrd,
{
    let (i1, i2) = partition(slice, p);
    let (less_than, rest) = slice.split_at_mut(i1);
    let (equal, greater_than) = rest.split_at_mut(i2 - i1);
    (less_than, equal, greater_than)
}